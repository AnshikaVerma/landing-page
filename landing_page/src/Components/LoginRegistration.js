import React, { useState } from "react";
import "./navbar.css";
import { GiHamburgerMenu } from "react-icons/gi";
import { BiSearchAlt2 } from "react-icons/bi";

import Logo from "./Logo";
import UserRegistration from './UserRegistration';
import UserLogin from './UserLogin';

const Navbar = () => {

  const [show, setShow] = useState(false);
  const handleShow = () => {
    setShow(true);
  }
  const handleClose = () => {
     setShow(false);
  }

  const [showLogin, setLoginShow] = useState(false);
  const handleLoginShow = () => {
    setLoginShow(true);
  }
  const handleLoginClose = () => {
     setLoginShow(false);
  }

  const [showMediaIcons, setShowMediaIcons] = useState(false);
  const [open, setOpen] = useState(false);
  return (
    <>
      <nav className="main-nav">
        {/* 1st logo part  */}
        <div className="logo">
          <h2>
            {/* <span>L</span>ogo */}
            <Logo/>
          </h2>
          {/* <Logo/> */}
        </div>

        {/* 2nd menu part  */}
        <div
          className={
            showMediaIcons ? "menu-link mobile-menu-link" : "menu-link"
          }>
            <ul>
                <li>
                  <div class="search">
                        <BiSearchAlt2/>
                        <input type="text" name="search" id="search" placeholder="Search this website"/>
                  </div>
              </li>
            </ul>
        </div>

        {/* 3rd social media links */}
        <div className="social-media">
          <ul className="social-media-desktop">

          <ul>
            <li>
              <button className="btn" id="btn_1" onClick={handleShow}>Register</button>

              {/* <Button variant="primary" onClick={handleShow} style={{width:"150px"}}>
                Registration
              </Button> */}
              <UserRegistration show={show} handleClose={handleClose}/>
            </li>
          </ul>

          <ul>
            <li>
              {/* <button className="btn" id="btn_1">Login</button>  */}
               <button className="btn" id="btn_1" onClick={handleLoginShow}>Login</button>
               <UserLogin showLogin={showLogin} handleLoginClose={handleLoginClose}/>
            </li>
          </ul>
          {/* hamburget menu start  */}
          <div className="hamburger-menu">
            <a href="#" onClick={() => setShowMediaIcons(!showMediaIcons)}>
              <GiHamburgerMenu />
            </a>
          </div>
          </ul>
        </div>
        <i
        className={open ? "fas fa-bars close" : "fas fa-bars open"}
        onClick={() => setOpen(true)}
      ></i>
      <i
        className={open ? "fas fa-times open" : "fas fa-times close"}
        onClick={() => setOpen(false)}
      ></i>
      </nav>
    </>
  );
};

export default Navbar;