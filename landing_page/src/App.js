import React from "react";
import Navbar from "./Components/Navbar";
import { Switch, Route } from "react-router-dom";

const App = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Navbar />
      </Route>

      <Route exact path="/user">
        <Navbar />
      </Route>
    </Switch>
  );
};

export default App;